import "./styles/index.scss";

const menuButton = document.querySelector(".nav--mobile");
const sliderContainer = document.querySelector(".content--bg");
const sliderArrowElement = document.querySelector(".slider__arrow");
localStorage.setItem('activeSlide', 'slide0');

const menuMobileToggle = (event) => {
    const target = event.target;
    const menuList = document.querySelector("#siteNav");
    if(menuList.classList.contains("nav--show")) {
        menuList.classList.add("nav--hidden");
        menuButton.classList.remove("nav--mobileShow");
        setTimeout(() => {
            menuList.classList.remove("nav--show");
            menuList.classList.remove("nav--hidden");
        }, 500);
    } else {
        menuList.classList.add("nav--show");
        menuButton.classList.add("nav--mobileShow");
    }
};

const sliderToggle = () => {
    let activeSlide = localStorage.getItem("activeSlide");
    sliderContainer.classList.value = 'content--bg';
    switch (activeSlide) {
        case 'slide0' : sliderContainer.classList.add('slide0');break;
        case 'slide1' : sliderContainer.classList.add('slide1');break;
        case 'slide2' : sliderContainer.classList.add('slide2');break;
    }
    console.log(activeSlide);
};

const sliderArrowToggle = (event) => {
  const target = event.target;
  const activeSlide = document.querySelector(".slider__item--active");
  switch (activeSlide.innerHTML) {
      case ('01'): activeSlide.innerHTML = '02';
                    localStorage.setItem('activeSlide', 'slide1');
                    sliderToggle();break;
      case ('02'): activeSlide.innerHTML = '03';
                    localStorage.setItem('activeSlide', 'slide2');
                    sliderToggle();break;
      case ('03'): activeSlide.innerHTML = '01';
                    localStorage.setItem('activeSlide', 'slide0');
                    sliderToggle();break;
  }
};

menuButton.addEventListener('click', menuMobileToggle);
sliderArrowElement.addEventListener('click', sliderArrowToggle);
